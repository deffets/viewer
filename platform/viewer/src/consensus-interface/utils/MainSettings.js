
import Cookies from 'universal-cookie';

class MainSettings {
  constructor() {
    this.cookies = new Cookies();
  }

  set(settingName, settingValue) {
    this.cookies.set(settingName, settingValue, { path: '/' });
  }
  
  get(settingName) {
    return (this.cookies.get(settingName));
  }
}

const SETTING_NAMES = {
  PACS_URL: 'PACS_URL',
};

export {SETTING_NAMES, MainSettings};

