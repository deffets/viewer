// In the use case where the viewer React application is rendered in a shadowDOM, it is necessary to resolve "getElementById" from the shadow DOM root
// see https://stackoverflow.com/questions/60763641/how-do-i-getelementbyid-from-inside-a-shadow-dom

export function getElementFromShadowRoot(shadowHostId) {
  if (!shadowHostId) {
    return document;
  } else {
    try {
      const elem = document.getElementById(shadowHostId);
      return elem.shadowRoot;
    } catch (e) {
      console.error(`No shadowHostId found ${shadowHostId} in the document: `, e);
      return document;
    }
  }
}
