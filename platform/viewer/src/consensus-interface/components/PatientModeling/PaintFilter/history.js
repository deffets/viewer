export default class History {
  static instance = null;
  history = {
    index: -1,
    snapshots: [],
    labels: []
  };
  undos = [];
  redos = [];
    
  static getInstance() {
    if (!History.instance)
      History.instance = new History();
      
    return this.instance;
  }
  
  addUndoRedo(undo, redo) {
    this.undos.push(undo);
    this.redos.push(redo);
  }
  
  removeUndoRedo(undo, redo) {
    let index = this.undos.indexOf(undo);
    
    if (index>-1)
      this.undos.splice(index, 1);
      
    index = this.redos.indexOf(redo);
    
    if (index>-1)
      this.redos.splice(index, 1);
  }
  
  canRedo() {
    return this.history.index < this.history.labels.length - 1;
  }
  
  canUndo() {
    return this.history.index > -1;
  }
  
  getHistory() {
    return this.history;
  }

  pushToHistory(snapshot, label) {
    console.log('Pushing to history');
    
    // Clear any "redo" info
    const spliceIndex = this.history.index + 1;
    const spliceLength = this.history.snapshots.length - this.history.index;
    this.history.snapshots.splice(spliceIndex, spliceLength);
    this.history.labels.splice(spliceIndex, spliceLength);

    // Push new snapshot
    this.history.snapshots.push(snapshot);
    this.history.labels.push(label);
    this.history.index++;
  }
  
  redo() {
    if (!this.redos.length || !this.canRedo())
      return;
    
    const redoLabel = this.history.labels[this.history.index + 1];
    const snapshot = this.history.snapshots[this.history.index + 1];
    
    this.history.index++;
    
    this.redos[0](redoLabel, snapshot);
  }
  
  resetHistory() {
    this.history.index = -1;
    this.history.snapshots = [];
    this.history.labels = [];
  }
  
  undo() {
    if (!this.undos.length || !this.canUndo())
      return;
      
    const snapshot = this.history.snapshots[this.history.index];
    this.history.index--;
    
    this.undos[0](snapshot);
  }
  
  undoAll() {
    while (this.canUndo())
      this.undo();
  }
}

