
const fusionColors = [
  {
    name: 'None',
    id: 'None',
    primary: [0.5, 0.5, 0.5],
    secondary: [0.5, 0.5, 0.5],
  },
  {
    name: 'White/Pink',
    id: 'White/Pink',
    primary: [0.5, 0.5, 0.5],
    secondary: [255/255, 192/255, 203/255],
  },
  {
    name: 'Red/White',
    id: 'Red/White',
    primary: [1, 0, 0],
    secondary: [0.5, 0.5, 0.5],
  },
  {
    name: 'Blue/Orange',
    id: 'Blue/Orange',
    primary: [0, 0, 1],
    secondary: [1, 165/255, 0],
  },
  {
    name: 'Magenta/Green',
    id: 'Magenta/Green',
    primary: [1, 0, 1],
    secondary: [0, 1, 0],
  },
  {
    name: 'White/Jet',
    id: 'White/Jet',
    primary: [0.5, 0.5, 0.5],
    secondary: [
      {pos: 0, color: [0,         0,    0.6667]},
      {pos: 0.1, color: [0,         0,    1.0000]},
      {pos: 0.2, color: [0,    0.3333,    1.0000]},
      {pos: 0.3, color: [0,    0.6667,    1.0000]},
      {pos: 0.4, color: [0,    1.0000,    1.0000]},
      {pos: 0.5, color: [0.3333,    1.0000,    0.6667]},
      {pos: 0.6, color: [0.6667,    1.0000,    0.3333]},
      {pos: 0.7, color: [1.0000,    1.0000,         0]},
      {pos: 0.8, color: [1.0000,    0.6667,         0]},
      {pos: 0.9, color: [1.0000,    0.3333,         0]},
      {pos: 1, color: [1.0000,         0,         0]}
    ]
  }
];

export default fusionColors;

