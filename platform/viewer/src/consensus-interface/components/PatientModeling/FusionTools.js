import React, { Component } from "react";
import { Icon } from '../../../../../ui/src/elements/Icon';
import ViewsApi from './Services/ViewsApi';
import fusionColors from "./colors.js";
import {SecondaryWindowWLSettings} from './ROIToolsSettings';
import newId from "../../utils/newid.js";
import { getElementFromShadowRoot } from '../../utils/shadowRoot.js';

import './styles.css';

//https://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb
function componentToHex(c) {
  var hex = c.toString(16);
  return hex.length == 1 ? "0" + hex : hex;
}

//https://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb
function rgbToHex(r, g, b) {
  r = Math.round(r); g = Math.round(g); b = Math.round(b);
  return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

//https://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb
function hexToRgb(hex) {
  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result ? [parseInt(result[1], 16),
    parseInt(result[2], 16),
    parseInt(result[3], 16)
  ] : null;
}

class TabLinks extends React.Component {
  render() {
    return (
      <button
        className={this.props.active ? 'tablinks' + ' active' : 'tablinks'}
        id={this.props.id}
        onClick={() => {
          this.props.onClick();
        }}
      >
        {' '}
        {this.props.value}{' '}
      </button>
    );
  }
}

export default class FusionTools extends React.Component {
  constructor(props) {
    super(props);
    this.nid = newId();
    this.shadowHostId = this.props.shadowHostId;
    this.state = {
      activeTool: null,
      currentColorID: props.currentColorID,
      settingWindow: null,
    };
    
    this.viewApi = ViewsApi.getInstance();
    
    this.secondaryColorListener = (event) => this.secondaryColorEventListener(event);
    this.primaryColorListener = (event) => this.primaryColorEventListener(event);
  }
  
  componentDidMount() {
    const elem = getElementFromShadowRoot(this.shadowHostId);
    elem.getElementById(this.nid + '__' + 'primarySecondarySlider').value = "50";
    
    this.componentDidUpdate();
  }
  
  componentDidUpdate() {
    const secondaryImage = this.props.imageDataSet.getImage(this.props.secondarySeriesInstanceUID);
    const elem = getElementFromShadowRoot(this.shadowHostId);
    if (secondaryImage) {
      const colors = secondaryImage.getImageProperties().getColors();
      let colorValue = rgbToHex(colors[1].color[0]*255, colors[1].color[1]*255, colors[1].color[2]*255);

      const inputS = elem.getElementById(this.nid + '__' + "secondaryColorId");
      inputS.addEventListener('input', this.secondaryColorListener);
      
      inputS.value = colorValue;
    }
    
    const image = this.props.imageDataSet.getImage(this.props.SeriesInstanceUID);
    
    if (image) {
      const colors = image.getImageProperties().getColors();
      let colorValue = rgbToHex(colors[1].color[0]*255, colors[1].color[1]*255, colors[1].color[2]*255);
      
      const inputP = elem.getElementById(this.nid + '__' + "primaryColorId");
      inputP.addEventListener('input', this.primaryColorListener);
      
      inputP.value = colorValue;
    }
  }
  
  secondaryColorEventListener(event) {
    const secondaryImage = this.props.imageDataSet.getImage(this.props.secondarySeriesInstanceUID);
    const elem = getElementFromShadowRoot(this.shadowHostId);
    if (secondaryImage) {
      const input = elem.getElementById(this.nid + '__' + "secondaryColorId");
      const color = hexToRgb(input.value);
      secondaryImage.getImageProperties().setColor([color[0]/255, color[1]/255, color[2]/255]);
    }
  }
  
  primaryColorEventListener(event) {
    const image = this.props.imageDataSet.getImage(this.props.SeriesInstanceUID);
    const elem = getElementFromShadowRoot(this.shadowHostId);
    if (image) {
      const input = elem.getElementById(this.nid + '__' + "primaryColorId");
      const color = hexToRgb(input.value);
      image.getImageProperties().setColor([color[0]/255, color[1]/255, color[2]/255]);
    }
  }
  
  adjustPrimarySecondary() {
    const elem = getElementFromShadowRoot(this.shadowHostId);
    const value = elem.getElementById(this.nid + '__' + "primarySecondarySlider").value/100;
    
    this.props.api.adjustPrimarySecondary(value);
  }
  
  changeColorID = event => {
    let color = null;
    color = fusionColors.find(color => color.id === event.target.value);
    
    const image = this.props.imageDataSet.getImage(this.props.SeriesInstanceUID);
    
    if (image)
      image.getImageProperties().setColor(color.primary);
    
    const secondaryImage = this.props.imageDataSet.getImage(this.props.secondarySeriesInstanceUID);

    if (secondaryImage)
      secondaryImage.getImageProperties().setColor(color.secondary);
    
    this.props.api.setCurrentColorID(event.target.value);
  };
  
  closeSettingWindow(){
	  this.setState({settingWindow: null});
  }
  
  toggleSettingsWindow() {
	  let windowSetting = null;
    console.log(`Click on toggleSettingsWindow`);
    switch(this.props.activeTool) {
      case "adjust":
        console.log(`[toggleSettingsWindow] case adjust `);
        //[GHERBI] 15-APR-2022 - TODO: shouldn't it be WindowWLSettings here, instead ?
        windowSetting = <SecondaryWindowWLSettings onClose={() => this.closeSettingWindow()} shadowHostId={this.shadowHostId}/>;
        break;
      case "adjustSecondary":        
        console.log(`[toggleSettingsWindow] case adjust Secondary`);
        windowSetting = <SecondaryWindowWLSettings onClose={() => this.closeSettingWindow()} shadowHostId={this.shadowHostId} />;
        break;
      }
    if(windowSetting !== null) {
      this.setState({settingWindow: windowSetting});
    }
  }
  
  componentWillUnmount() {
    const elem = getElementFromShadowRoot(this.shadowHostId);
    const input = elem.getElementById(this.nid + '__' + "secondaryColorId");
    if(input)
      input.removeEventListener('input', this.secondaryColorListener);
    
    const inputP = elem.getElementById(this.nid + '__' + "primaryColorId");
    if(inputP)
      inputP.removeEventListener('input', this.primaryColorListener);
  }
  
  render() {
    const tdStyle = {
      width: "20px",
      height: "20px",
      border: "none",
      cursor: "pointer",
      border: "none",
      outline: "none",
      padding: 0,
    };
    
    
    let optionMenu = null;
    let optionLabel = null;
    let iconName = null;
    switch(this.props.activeTool) {
      case 'adjust':
        optionLabel = 'W W/L';
        iconName = 'level';
        break;
      case 'adjustSecondary':
        optionLabel = 'W W/L';
        iconName = 'level';
        break;
      case 'label':
        break;
    }

    if (optionLabel) {
      optionMenu = (
        <table>
          <tbody>
            <tr onClick={() => this.toggleSettingsWindow()}>
              <td>
                <Icon className="medIcon" name={iconName} />
              </td>
              <td>{optionLabel + ' options'}</td>
            </tr>
          </tbody>
        </table>
      );
    }

    const colorOptions = fusionColors.map(color => {
      return (
        <option key={color.id} value={color.id}>
          {color.name}
        </option>
      );
    });
    
    return (
      <div className="ROITools">
        <div className="drawTools">
          <div className="space20px"></div>
          <Icon
            className="medIcon"
            name="times"
            onClick={() => this.props.api.endAll()}
          />
          <div className="space20px"></div>
          <div className="space20px"></div>
          <div className="slidecontainer">
            <input
              onInput={() => this.adjustPrimarySecondary()}
              type="range"
              min="0"
              max="100"
              id={this.nid + '__' + 'primarySecondarySlider'}
            />
            <table>
              <tbody>
                <tr>
                  <td>Primary</td>
                  <td style={{ textAlign: 'right', width: '100%' }}>
                    Secondary
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="space20px"></div>
          <table>
            <tbody>
              <tr
                className={
                  this.props.activeTool == 'adjust'
                    ? 'roiTool' + ' active'
                    : 'roiTool'
                }
                onClick={() => this.props.api.startAdjust()}
              >
                <td>
                  <Icon className="smallIcon" name="level" />
                </td>
                <td>Primary</td>
              </tr>
              <tr
                className={
                  this.props.activeTool == 'adjustSecondary'
                    ? 'roiTool' + ' active'
                    : 'roiTool'
                }
                onClick={() => this.props.api.startAdjustSecondary()}
              >
                <td>
                  <Icon className="smallIcon" name="level" />
                </td>
                <td>Secondary</td>
              </tr>
            </tbody>
          </table>
          <div className="space20px"></div>
          <table>
            <tbody>
              <tr>Color scheme:</tr>
              <tr>
                <td>
                  <select
                    value={this.props.currentColorID}
                    onChange={this.changeColorID}
                  >
                    {colorOptions}
                  </select>
                </td>
              </tr>
            </tbody>
          </table>
          <div className="space20px"></div>
          <table>
            <tbody>
              <tr>
                <td>
                  <input
                    id={this.nid + '__' + 'primaryColorId'}
                    style={tdStyle}
                    type="color"
                  />
                </td>
                <td>Primary color</td>
              </tr>
              <tr>
                <td>
                  <input
                    id={this.nid + '__' + 'secondaryColorId'}
                    style={tdStyle}
                    type="color"
                  />
                </td>
                <td>Secondary color</td>
              </tr>
            </tbody>
          </table>
          <div className="space20px"></div>
          <table>
            <tbody>
              <tr
                className={
                  this.props.activeTool == 'crossHairs'
                    ? 'roiTool' + ' active'
                    : 'roiTool'
                }
                onClick={() => this.props.api.startCrossHairs()}
              >
                <td>
                  <Icon className="smallIcon" name="crosshairs" />
                </td>
                <td>Crosshairs</td>
              </tr>
              <tr
                className={
                  this.props.activeTool == 'zoom'
                    ? 'roiTool' + ' active'
                    : 'roiTool'
                }
                onClick={() => this.props.api.startZoom()}
              >
                <td>
                  <Icon className="smallIcon" name="magnifyingGlass" />
                </td>
                <td>Zoom</td>
              </tr>
            </tbody>
          </table>
          <div className="space20px"></div>
          <div className="space20px"></div>
          {optionMenu}
        </div>
        {this.state.settingWindow}
      </div>
    );
  }
}
