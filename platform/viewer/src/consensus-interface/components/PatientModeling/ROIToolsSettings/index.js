import WindowWLSettings from './WindowWLSettings';
import PaintSettings from './PaintSettings';
import LayoutSettings from './LayoutSettings';
import SecondaryWindowWLSettings from './SecondaryWindowWLSettings';

export {WindowWLSettings, PaintSettings, LayoutSettings, SecondaryWindowWLSettings};
