import React from "react";
import presets from '../presets.js';
import "./ModalStyle.css";
import ViewsApi from '../Services/ViewsApi';
import newId from "../../../utils/newid.js";
import { getElementFromShadowRoot } from '../../../utils/shadowRoot.js';


export default class SecondaryWindowWLSettings extends React.Component {
  constructor(props) {
    super(props);
    this.nid = newId();
    this.shadowHostId = this.props.shadowHostId;
    this.viewApi = ViewsApi.getInstance();
    
    this.viewApiListener = (whatChange) => {this.onPropertyChange(whatChange)};
    console.log('[SecondaryWindowWLSettings] this.viewApiListener', this.viewApiListener);
    
    this.viewApi.addSecondaryImagePropertiesListener(this.viewApiListener);
    
    this.initialWWL = this.viewApi.getSecondaryWWL().slice();
    this.initialWWL[0] = Math.round(this.initialWWL[0]);
    this.initialWWL[1] = Math.round(this.initialWWL[1]);
    
    this.initialPresetID = null;
    if (this.viewApi.getTransferFunction())
      this.initialPresetID = this.viewApi.getTransferFunction().id;
    
    this.state = {
      isManual: false,
      currentPresetID: this.initialPresetID,
      windowWidthValue: this.initialWWL[0],
      windowLevelValue: this.initialWWL[1],
    };
  }
  
  componentWillUnmount() {
    this.viewApi.removeSecondaryImagePropertiesListener(this.viewApiListener);
  }
  
  onPropertyChange(whatChange) {
    if (whatChange=="wwl") {
      const wwl = this.viewApi.getSecondaryWWL().slice();
      if (wwl[0] == this.state.windowWidthValue && wwl[1] == this.state.windowLevelValue){
        return;
      }
      this.setState({
        windowWidthValue: Math.round(wwl[0]),
        windowLevelValue: Math.round(wwl[1]),
      });
    }
  }
  
  changePresetID = event => {
    let preset = null;
    preset = presets.find(preset => preset.id === event.target.value);
    
    this.viewApi.setTransferFunction(preset);
    
    this.setState({currentPresetID: event.target.value});
  };
  
  changeWindowWidth = event => {
    this.viewApi.setSecondaryWWL(event.target.value, this.state.windowLevelValue);
    this.setState({windowWidthValue: event.target.value});
  };
  
  changeWindowLevel = event => {
    this.viewApi.setSecondaryWWL(this.state.windowWidthValue, event.target.value);
    this.setState({windowLevelValue: event.target.value});
  };
  
  reset() {
    this.viewApi.setSecondaryWWL(this.initialWWL[0], this.initialWWL[1]);
    
    this.setState({
      windowWidthValue: this.initialWWL[0],
      windowLevelValue: this.initialWWL[1],
    });
  }
  adjustWL() {
    const elem = getElementFromShadowRoot(this.shadowHostId);
    const width_ = elem.getElementById(this.nid + '__' + 'widthNumber').value;
    const level_ = elem.getElementById(this.nid + '__' + 'levelNumber').value;
    this.viewApi.setSecondaryWWL(width_, level_);
    this.setState({
      windowWidthValue: width_,
      windowLevelValue: level_,
    });
    // console.log(`this.state (2nd) = \n${JSON.stringify(this.state)}`);
  }
  
  render() {  
    const presetsOptions = presets.map(preset => {
      return (
        <option key={preset.id} value={preset.id}>
          {preset.name}
        </option>
      );
    });
  
    return (
      <div className="modal" id="modal">
        <h2>WindowWL Settings (Secondary Image)</h2>
        <div className="content">
      <div className="flex-container-column">
        <select value={this.state.currentPresetID} onChange={this.changePresetID}>
          {presetsOptions}
        </select>
        <div className="flex-container-row">
          <label>Window width</label>
          <input type="range" id={this.nid + '__' + "widthSlider"} min="0" max="1800" value={this.state.windowWidthValue} onChange={this.changeWindowWidth} />
          <label>{this.state.windowWidthValue}</label>
        </div>
        <div className="flex-container-row">
          <label>Window level</label>
          <input type="range" id={this.nid + '__' + "levelSlider"} min="-1000" max="3000" value={this.state.windowLevelValue} onChange={this.changeWindowLevel} />
          <label>{this.state.windowLevelValue}</label>
        </div>
        <div className = "flex-container-row">
          <label>WW | WL</label>
          <input style={{"marginLeft": "1em"}} type="number" id={this.nid + '__' + "widthNumber"} maxLength="4" size="4" ></input>
          <input style={{"marginLeft": "1em"}} type="number" id={this.nid + '__' + "levelNumber"} maxLength="4" size="4" ></input>
          <button style={{"marginLeft": "1em"}} className="applyButton" onClick={() => this.adjustWL()}>Adjust</button>
        </div>
      </div>
    </div>
        <div className="actions">
        <button className="applyButton" onClick={() => this.reset()}>
            Reset
          </button>
          <button className="closeButton" onClick={this.props.onClose}>
            Close
          </button>
        </div>
      </div>
    );
  }
}
