import React from "react";
import "./ModalStyle.css";
import ViewsApi from '../Services/ViewsApi'

export default class PaintSettings extends React.Component {
  constructor(props) {
    super(props);
    
    this.viewApi = ViewsApi.getInstance();
    
    this.state = {
      currentRadius: this.viewApi.getRadius(),
    };
  }
  
  changePaintRadius = event => {
    this.setState({currentRadius: event.target.value/2.0});
  };
  
  apply(){
    if(this.viewApi.setPaintRadius(this.state.currentRadius))
    this.props.onClose();
  };
  
  render() {  
    return (
      <div className="modal" id="modal">
        <h2>Paint Settings</h2>
        <div className="content">
          <div className="flex-container-column">
            <div className="flex-container-row">
              <label>Diameter</label>
              <input type="range" id="paintRadius" min="1" max="50" value={this.state.currentRadius*2.0} onChange={this.changePaintRadius} />
              <label>{this.state.currentRadius*2.0}</label>
            </div>
          </div>
        </div>
        <div className="actions">
          <button className="applyButton" onClick={() => this.apply()}>
            apply
          </button>
          <button className="closeButton" onClick={this.props.onClose}>
            close
          </button>
        </div>
      </div>
    );
  }
}
