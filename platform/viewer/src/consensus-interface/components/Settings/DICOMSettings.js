
import React, { Component } from "react";
import {SETTING_NAMES, MainSettings} from '../../utils/MainSettings.js';
import { getElementFromShadowRoot } from '../../utils/shadowRoot.js';

import './styles.css'

export default class PatientDataManagement extends React.Component {
  constructor(props) {
    super(props);
    console.log('[PatientDataManagement] props.shadowHostId = ? ', this.props)
    this.shadowHostId = this.props.shadowHostId;

    this.settings = new MainSettings();
    
    this.state = {PACSURL: this.settings.get(SETTING_NAMES.PACS_URL)};
  }
  
  onSaveChanges() {
    const elem = getElementFromShadowRoot(this.shadowHostId);
    const urlInput = elem.getElementById(SETTING_NAMES.PACS_URL);

    this.settings.set(SETTING_NAMES.PACS_URL, urlInput.value);
    
    this.setState({PACSURL: this.settings.get(SETTING_NAMES.PACS_URL)});
  }
  
  render() {
    return (
      <div>
        <form>
          <label for={SETTING_NAMES.PACS_URL}>PACS URL: </label>
          <input type="text" id={SETTING_NAMES.PACS_URL} name={SETTING_NAMES.PACS_URL} defaultValue={this.settings.get(SETTING_NAMES.PACS_URL)}></input><br></br><br></br>
          <input type="submit" value="Save changes" onClick={() => this.onSaveChanges()}></input>
        </form>
      </div>
    );
  }
}

