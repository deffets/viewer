
import DICOMWeb from '../../../../core/src/DICOMWeb/'; //TODO: use dcmjs instead

import {api} from "dicomweb-client";
import dcmjs from 'dcmjs';


export default class DicomDataLoader {
  static instance = null;
  pacsURL = null;
  client = null;
  enaled = true;
  ongoingFetchNb = 0;
  
  enable() {
    this.enaled = true;
  }
  
  async disable() {
    this.enaled = false;
    
    await this.waitUntilIsDisabled();
  }
  
  async waitUntilIsDisabled() {
    const sleep = (ms) => new Promise(resolve => setTimeout(resolve, ms));
    
    let elapsedTime = 0
    const sleepPeriod = 50;
    while(this.ongoingFetchNb) {
      await sleep(sleepPeriod);
      
      elapsedTime += sleepPeriod
      
      if (elapsedTime>1000)
        return;
    }
    
    if(!this.enaled) // if was intentionnaly disabled, wait 10 ms more to be sure return statement occured.
      sleep(10);
  }
  
  setPACSURL(pacsURL) {
    if(pacsURL==this.pacsURL)
      return;
      
    this.pacsURL = pacsURL;
    this.client = new api.DICOMwebClient({url: pacsURL});
  }
  
  static getInstance(){
    if (!DicomDataLoader.instance)
      DicomDataLoader.instance = new DicomDataLoader();
      
    return this.instance;
  }
  
  async getInstanceMetaData(StudyInstanceUID, SeriesInstanceUID, SOPInstanceUID) {
    if (!this.enaled)
      return null;
      
    this.ongoingFetchNb++;

    let dicomData = null;
    try {
      const options = {
        studyInstanceUID:StudyInstanceUID, 
        seriesInstanceUID:SeriesInstanceUID,
        sopInstanceUID:SOPInstanceUID
      };
    
      const data = await this.client.retrieveInstance(options);
    
      dicomData = dcmjs.data.DicomMessage.readFile(data);    
    } finally {
      this.ongoingFetchNb--;
    }
    
    return (dcmjs.data.DicomMetaDictionary.naturalizeDataset(dicomData.dict));
  }
  
  async getRTDoseData(StudyInstanceUID, SeriesInstanceUID, SOPInstanceUID, seriesMetaData) {
    // console.log("GET RT DOSE DATA")
    if (!this.enaled)
      return null;
      
    this.ongoingFetchNb++;
    
    let newData = null;
    try {
      const options = {
        studyInstanceUID:StudyInstanceUID, 
        seriesInstanceUID:SeriesInstanceUID,
        sopInstanceUID:SOPInstanceUID
      };
      
      if (!seriesMetaData)
        seriesMetaData = await this.getSeriesMetaData(url, StudyInstanceUID, SeriesInstanceUID); // We could retrieve them directly from client.retrieveInstance
      
      const data = await this.client.retrieveInstance(options);
      // console.log('seriesMetaData' + seriesMetaData);

      let dataArray = null;
      if (seriesMetaData[0].BitsAllocated==16)
        dataArray = new Uint16Array(data);
      if (seriesMetaData[0].BitsAllocated==32)
        dataArray = new Uint32Array(data);
      
      console.log('NumberOfFrames: ' + seriesMetaData[0].NumberOfFrames)
      
      newData = dataArray.slice(dataArray.length-seriesMetaData[0].Rows*seriesMetaData[0].Columns*seriesMetaData[0].NumberOfFrames, dataArray.length);
      
      
      // console.log(`seriesMetaData : ${seriesMetaData[0]}`);
      
      if (seriesMetaData[0].DoseGridScaling) {
        // console.log(`Passed by DoseGridScaling Mapping with factor ${seriesMetaData[0].DoseGridScaling}`);
        newData = newData.map(elem => elem * seriesMetaData[0].DoseGridScaling);
      }
    } finally {
      this.ongoingFetchNb--;
    }

    return new Int16Array(newData);
  }

  async getInstanceData(StudyInstanceUID, SeriesInstanceUID, SOPInstanceUID, seriesMetaData) {
    if (!this.enaled)
      return null;
      
    this.ongoingFetchNb++;
    
    let newData = null;
    try {
      const options = {
        studyInstanceUID:StudyInstanceUID, 
        seriesInstanceUID:SeriesInstanceUID,
        sopInstanceUID:SOPInstanceUID
      };
      
      if (!seriesMetaData)
        seriesMetaData = await this.getSeriesMetaData(url, StudyInstanceUID, SeriesInstanceUID); // We could retrieve them directly from client.retrieveInstance
      
      const data = await this.client.retrieveInstance(options);
      const data16 = new Int16Array(data);
      
      newData = data16.slice(data16.length-seriesMetaData[0].Rows*seriesMetaData[0].Columns, data16.length);
      
      if (seriesMetaData[0].hasOwnProperty("RescaleIntercept") && seriesMetaData[0].RescaleSlope)
        newData = newData.map(elem => elem*seriesMetaData[0].RescaleSlope + seriesMetaData[0].RescaleIntercept);
    } finally {
      this.ongoingFetchNb--;
    }
    
    return newData;
  }

  async getInstancesDataFromSeriesMetaData(seriesMetaData, progressFunc) {
    this.ongoingFetchNb++;
    
    let data = null;
    try {
      if (seriesMetaData[0].modalities == "RTDOSE") {

        let instanceMetada = seriesMetaData[0];
        data = new Int16Array(await this.getRTDoseData(instanceMetada.StudyInstanceUID, instanceMetada.SeriesInstanceUID, instanceMetada.SOPInstanceUID, seriesMetaData));
      } else {
        data = new Int16Array(seriesMetaData[0].Rows*seriesMetaData[0].Columns*seriesMetaData.length);
        // metadata already sorted by getSeriesMetaData
        let ind = 0;
        for (let i = 0; i < seriesMetaData.length; i++) {
          if (!this.enaled) {
            this.ongoingFetchNb--;
            return null;
          }
          console.log('Loading instance ' + (i+1) + '/' + seriesMetaData.length + ' ' + seriesMetaData[0].modalities);
          let instanceMetada = seriesMetaData[i];
          let instanceData = await this.getInstanceData(instanceMetada.StudyInstanceUID, instanceMetada.SeriesInstanceUID, instanceMetada.SOPInstanceUID, seriesMetaData);
          data.set(instanceData, ind);
          ind = ind + instanceData.length;
          progressFunc((i + 1) / seriesMetaData.length);
        }
      }
    } finally {
      this.ongoingFetchNb--;
    }
    
    return data;
  }
  
  async getSEGData(StudyInstanceUID, SeriesInstanceUID, SOPInstanceUID) {
    if (!this.enaled)
      return null;
    
    this.ongoingFetchNb++;
    
    let res = null;
    try {
      res = await this.getInstanceMetaData(StudyInstanceUID, SeriesInstanceUID, SOPInstanceUID);
    } finally {
      this.ongoingFetchNb--;
    }
    
    return res;
  }

  async getSeriesData(StudyInstanceUID, SeriesInstanceUID, progressFunc) {
    if (!this.enaled)
      return {data: null, metaData:null};
    
    this.ongoingFetchNb++;
    
    let seriesMetaData = null;
    let data = null;
    try {
      seriesMetaData = await this.getSeriesMetaData(StudyInstanceUID, SeriesInstanceUID);
      data = await this.getInstancesDataFromSeriesMetaData(seriesMetaData, progressFunc);
    } finally {
      this.ongoingFetchNb--;
    }
    
    return {data, metaData:seriesMetaData};
  }

  async getSeriesMetaData(StudyInstanceUID, SeriesInstanceUID) {
    if (!this.enaled)
      return null;
    
    this.ongoingFetchNb++;
    
    let res = null;
    try {
      const options = {
        studyInstanceUID:StudyInstanceUID, 
        seriesInstanceUID:SeriesInstanceUID
      };
      
      const seriesMetaData = await this.client.retrieveSeriesMetadata(options);
      res = resultDataToStudies(seriesMetaData, 'getSeriesMetaData').slice();
      //res.sort((metadata1, metadata2) => metadata1.InstanceNumber > metadata2.InstanceNumber ? 1 : -1); // We have to return a real number and not a boolean in chrome!
      res.sort((metadata1, metadata2) => metadata1.SliceLocation < metadata2.SliceLocation ? 1 : -1); // We have to return a real number and not a boolean in chrome!
    } finally {
      this.ongoingFetchNb--;
    }
    
    return res;
  }
  
  async getStudies() {
    if (!this.enaled)
      return null;
      
    this.ongoingFetchNb++;
    
    let res = null;
    try {
      const resultData = await this.client.searchForStudies();
      res = resultDataToStudies(resultData, 'getStudies');
    } finally {
      this.ongoingFetchNb--;
    }
    
    return res;
  }
  
  async getStudyMetaData(StudyInstanceUID) {
    if (!this.enaled)
      return null;
      
    this.ongoingFetchNb++;
    
    let res = null;
    try {
      const options = {studyInstanceUID:StudyInstanceUID};
      
      const metaData = await this.client.retrieveStudyMetadata(options);
      
      res = resultDataToStudies(metaData, 'getStudyMetaData');
    } finally {
      this.ongoingFetchNb--;
    }
    
    return res;
  }
  
  sendSEGData(metaData) {
    console.log('Not implemented yet - Cannot send:');
    console.log(metaData);
  }
}

function resultDataToStudies(resultData, source) {
  const studies = [];

  if (!resultData || !resultData.length)
    return;

  resultData.forEach(study => {
    let PixelSpacing = null;
    let ImagePositionPatient = null;
    let SliceThickness = null;
    let ImageOrientationPatient = null;
    let RescaleIntercept = null;
    let RescaleSlope = null;
    let InstanceCreationDate = null;
    let InstanceNumber = null;
    let SliceLocation = null;
    let NumberOfFrames = null;
    let BitsAllocated = null;
    let DoseGridScaling = null;
    
    if (typeof study['00280030'] !== 'undefined') {
      PixelSpacing = study['00280030'].Value.slice()
      if (PixelSpacing[0].indexOf('\\') > -1)
        PixelSpacing = PixelSpacing[0].split("\\").slice();
        
      PixelSpacing = PixelSpacing.map(elem => parseFloat(elem));
    }
    
    if (typeof study['00200032'] !== 'undefined') {
      ImagePositionPatient = study['00200032'].Value.slice()
      if (ImagePositionPatient[0].indexOf('\\') > -1)
        ImagePositionPatient = ImagePositionPatient[0].split("\\").slice();
     
      ImagePositionPatient = ImagePositionPatient.map(elem => parseFloat(elem));
    }
      	
    if (typeof study['00200037'] !== 'undefined') {
      ImageOrientationPatient = study['00200037'].Value.slice()
      if (ImageOrientationPatient[0].indexOf('\\') > -1)
        ImageOrientationPatient = ImageOrientationPatient[0].split("\\").slice();
      	
      ImageOrientationPatient = ImageOrientationPatient.map(elem => parseInt(elem));
    }
    
    if (typeof study['00280008'] !== 'undefined') {
      NumberOfFrames = parseInt(study['00280008'].Value[0]);
    }
    if (typeof study['00280100'] !== 'undefined') {
      BitsAllocated = parseInt(study['00280100'].Value[0]);
    }

    if (typeof study['3004000E'] !== 'undefined') {
      DoseGridScaling = parseFloat(study['3004000E'].Value[0]);
    }

    if (study['00180050'])
      SliceThickness = parseFloat(study['00180050'].Value[0]);
    if (study['00281052'])
      RescaleIntercept = parseFloat(study['00281052'].Value[0]);
    if (study['00281053'])
      RescaleSlope = parseFloat(study['00281053'].Value[0]);
    if (study['00080012'])
      InstanceCreationDate = DICOMWeb.getString(study['00080012']);
    if (study['00200013'])
      InstanceNumber = parseInt(study['00200013'].Value[0]);
    if (study['00201041']) {
      SliceLocation = parseInt(study['00201041'].Value[0]);
    }

    studies.push({
      StudyInstanceUID: DICOMWeb.getString(study['0020000D']),
      // 00080005 = SpecificCharacterSet
      StudyDate: DICOMWeb.getString(study['00080020']),
      StudyTime: DICOMWeb.getString(study['00080030']),
      AccessionNumber: DICOMWeb.getString(study['00080050']),
      referringPhysicianName: DICOMWeb.getString(study['00080090']),
      // 00081190 = URL
      PatientName: DICOMWeb.getName(study['00100010']),
      PatientID: DICOMWeb.getString(study['00100020'])? DICOMWeb.getString(study['00100020']) : "Null",
      PatientBirthdate: DICOMWeb.getString(study['00100030']),
      patientSex: DICOMWeb.getString(study['00100040']),
      studyId: DICOMWeb.getString(study['00200010']),
      numberOfStudyRelatedSeries: DICOMWeb.getString(study['00201206']),
      numberOfStudyRelatedInstances: DICOMWeb.getString(study['00201208']),
      StudyDescription: DICOMWeb.getString(study['00081030']),
      SeriesDescription: DICOMWeb.getString(study['0008103E']),
      // Modality: DICOMWeb.getString(study['00080060']),
      // ModalitiesInStudy: DICOMWeb.getString(study['00080061']),
      modalities: DICOMWeb.getString(
        DICOMWeb.getModalities(study['00080060'], study['00080061'])
      ),
      SeriesInstanceUID: DICOMWeb.getString(study['0020000E']),
      PatientPosition: DICOMWeb.getString(study['00185100']),
      Manufacturer: DICOMWeb.getString(study['00080070']),
      SOPInstanceUID: DICOMWeb.getString(study['00080018']),
      Rows: study['00280010'] ? parseInt(study['00280010'].Value[0]) : null,
      Columns: study['00280011'] ? parseInt(study['00280011'].Value[0]) : null,

      BitsAllocated,

      InstanceNumber,
      PixelSpacing,
      ImagePositionPatient,
      ImageOrientationPatient,
      SliceThickness,
      RescaleIntercept,
      RescaleSlope,
      InstanceCreationDate,
      SliceLocation,
      NumberOfFrames,
      DoseGridScaling,
    });
  });
  
  return(studies);
}
